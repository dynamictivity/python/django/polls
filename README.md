# Polls App

This is the polls app, created by following the Django tutorial: <https://docs.djangoproject.com/en/2.1/intro/tutorial01/>

## Project Development Setup

Here's how to setup your environment

### Local Development

1. Follow the instructions to setup pipenv on your local system: <https://pipenv.readthedocs.io/en/latest/>
2. Run `$ pipenv install`
3. Run `$ pipenv shell`
4. Run `$ python manage.py migrate`
5. Run `$ python manage.py createsuperuser`

### Vagrant Development

TBD

## Development Continuation

### Models and Schema

If you make changes to the models/schema:

1. Run `$ python manage.py makemigrations polls`
2. Run `$ python manage.py sqlmigrate polls 0001`
   - `0001` above is an example
3. Run `$ python manage.py migrate`

### Admin

TBD

### Testing

#### Basic Testing

1. Run `$ python manage.py test polls`

#### Test Client

1. Run `$ python manage.py shell`

```python
>>> from django.test.utils import setup_test_environment
>>> setup_test_environment()
```

```python
>>> from django.test import Client
>>> # create an instance of the client for our use
>>> client = Client()
```

```python
>>> # get a response from '/'
>>> response = client.get('/')
Not Found: /
>>> # we should expect a 404 from that address; if you instead see an
>>> # "Invalid HTTP_HOST header" error and a 400 response, you probably
>>> # omitted the setup_test_environment() call described earlier.
>>> response.status_code
404
>>> # on the other hand we should expect to find something at '/polls/'
>>> # we'll use 'reverse()' rather than a hardcoded URL
>>> from django.urls import reverse
>>> response = client.get(reverse('polls:index'))
>>> response.status_code
200
>>> response.content
b'\n    <ul>\n    \n        <li><a href="/polls/1/">What&#39;s up?</a></li>\n    \n    </ul>\n\n'
>>> response.context['latest_question_list']
<QuerySet [<Question: What's up?>]>
```
